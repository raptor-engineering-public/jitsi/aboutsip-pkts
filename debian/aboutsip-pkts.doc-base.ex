Document: aboutsip-pkts
Title: Debian aboutsip-pkts Manual
Author: <insert document author here>
Abstract: This manual describes what aboutsip-pkts is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/aboutsip-pkts/aboutsip-pkts.sgml.gz

Format: postscript
Files: /usr/share/doc/aboutsip-pkts/aboutsip-pkts.ps.gz

Format: text
Files: /usr/share/doc/aboutsip-pkts/aboutsip-pkts.text.gz

Format: HTML
Index: /usr/share/doc/aboutsip-pkts/html/index.html
Files: /usr/share/doc/aboutsip-pkts/html/*.html
