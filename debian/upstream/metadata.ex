# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/aboutsip-pkts/issues
# Bug-Submit: https://github.com/<user>/aboutsip-pkts/issues/new
# Changelog: https://github.com/<user>/aboutsip-pkts/blob/master/CHANGES
# Documentation: https://github.com/<user>/aboutsip-pkts/wiki
# Repository-Browse: https://github.com/<user>/aboutsip-pkts
# Repository: https://github.com/<user>/aboutsip-pkts.git
